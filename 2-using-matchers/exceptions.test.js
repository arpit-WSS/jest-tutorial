// to check whether a function throws an error use "toThrow"

// [Note]: the function that throws and error should be invoked wrppaed in function.

function expectError() {
	throw new Error('File not found');
}

test('', () => {
	expect(() => expectError()).toThrow();
	expect(() => expectError()).toThrow(Error);

	// You can also use the exact error message or a regexp
	expect(() => compileAndroidCode()).toThrow('you are using the wrong JDK');
	expect(() => compileAndroidCode()).toThrow(/JDK/);
});
