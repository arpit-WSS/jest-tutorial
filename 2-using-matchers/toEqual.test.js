// toBe uses "Object.is" for exact equality.
// toEqual is used for checking the value of object

test('Object Assignment', () => {
	const dataOb = { one: 1 };

	dataOb['two'] = 2;

	expect(dataOb).toEqual({ one: 1, two: 2 });
});
