test('null', () => {
	const n = null;
	expect(n).toBeNull(); //matches if null
	expect(n).toBeDefined();
	expect(n).not.toBeUndefined(); //matches undefined
	expect(n).not.toBeTruthy(); // matches anything that "if" treats as true
	expect(n).toBeFalsy(); //matches anything that "if" treats as false
});

test('zero', () => {
	const z = 0;
	expect(z).not.toBeNull();
	expect(z).toBeDefined();
	expect(z).not.toBeUndefined();
	expect(z).not.toBeTruthy();
	expect(z).toBeFalsy();
});
