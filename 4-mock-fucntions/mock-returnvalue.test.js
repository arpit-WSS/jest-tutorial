const mockfn1 = jest.fn();

//inject values during test.
test('injecting values', () => {
	mockfn1.mockReturnValueOnce(45);
	mockfn1.mockReturnValueOnce('x');

	console.log(mockfn1(), mockfn1());
});

// mock functions are useful for code using functions based on continuation passing style.
// eg: map(), filter(),etc

test('testing filter function using mock', () => {
	const mockfn2 = jest.fn();
	mockfn2.mockReturnValueOnce(true).mockReturnValueOnce(false);
	// mockfn2();
	// mockfn2();

	// expect(mockfn2.mock.results[0].value).toBe(true);
	// expect(mockfn2.mock.results[1].value).not.toBeTruthy();

	const list_a = [ 11, 12 ];
	const result = list_a.filter((value) => mockfn2(value));
	console.log(result);
});
