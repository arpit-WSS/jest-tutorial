import axios from 'axios';

class Users {
	static all() {
		return axios.get('/users.json').then((resp) => resp.data);
	}
}

jest.mock('axios');

test('should fetch users', () => {
	const users = [ { name: 'bob' } ];
	const resp = { data: users };
	axios.get.mockResolveValue(resp);

	return Users.all().then((data) => expect(data).toEqual(users));
});
