function forEach(items, callback) {
	for (let i = 0; i < items.length; i++) {
		callback(items[i]);
	}
}

//create mock function

const mockfn = jest.fn((x) => 42 + x);
forEach([ 0, 1 ], mockfn);
const a = new mockfn();
const b = {};
const bound = mockfn.bind(b);
bound();

test('checking mock function', () => {
	expect(mockfn.mock.calls.length).toBe(4);
	//first argument of the mock function shoulf be 0 in first call
	expect(mockfn.mock.calls[0][0]).toBe(0);

	//first argument of the mock function shoulf be 1 in second call
	expect(mockfn.mock.calls[1][0]).toBe(1);
});

// .mock property of mock function

test('checking mock function', () => {
	//check for instances
	console.log(mockfn.mock.instances.length);
	expect(mockfn.mock.instances.length).toBe(4);

	//check for first argument of the last call
	expect(mockfn.mock.lastCall[0]).toBe(undefined);

	//check for the return value of the first call
	expect(mockfn.mock.results[0].value).toBe(42);
});
