//test.js
import { baz, bar, foo } from 'foo-bar';

jest.mock('./foo-bar', () => {
	const originalModule = jest.requireActual('foo-bar');

	//Mock the default export and named export 'foo'
	return {
		__esModule: true,
		...originalModule,
		baz: jest.fn(() => 'mocked baz'),
		foo: 'mocked foo'
	};
});

test('test the partial module', () => {
	const defaultExportResult = baz();
	expect(defaultExportResult).toBe('mocked baz');
	expect(defaultExport).toHaveBeenCalled();

	expect(foo).toBe('mocked foo');
	expect(bar).toBe('bar');
});
