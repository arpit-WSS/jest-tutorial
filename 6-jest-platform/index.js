// import { getChangedFilesForRoots } from 'jest-changed-files';
const { getChangedFilesForRoots } = require('jest-changed-files');

getChangedFilesForRoots([ '../6-jest-platform' ], {
	lastCommit: true
}).then((result) => console.log(result.changedFiles));
