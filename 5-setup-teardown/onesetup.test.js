let city = [];

function initializeCityDatabase() {
	city.push('Vienna');
	city.push('San Juan');
}

function clearCityDatabase() {
	return (city = []);
}

function isCity(name) {
	return city.includes(name);
}

beforeAll(() => {
	return initializeCityDatabase();
});

afterAll(() => {
	return clearCityDatabase();
});

test('city database has Vienna', () => {
	expect(isCity('Vienna')).toBeTruthy();
});

test('city database has San Juan', () => {
	expect(isCity('San Juan')).toBeTruthy();
});

test('city database has San Juan', () => {
	expect(city.length).toBe(2);
});
