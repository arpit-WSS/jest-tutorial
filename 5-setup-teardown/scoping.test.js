let city = [];
let food = {};

function initializeCityDatabase() {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			city.push('Vienna');
			city.push('San Juan');
			resolve();
		}, 100);
	});
}

function clearCityDatabase() {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			city = [];
			resolve();
		}, 100);
	});
}

function isCity(name) {
	return city.includes(name);
}

function isValidCityFoodPair(city, foodname) {
	return food[city] === foodname;
}
function initializeFoodDatabase() {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			food['Vienna'] = 'Wiener Schnitzel';
			food['San Juan'] = 'Mofongo';
			resolve();
		}, 100);
	});
}

// Applies to all tests in this file
beforeEach(() => {
	console.log('1-beforeEach');
	return initializeCityDatabase(); // this will run before the two tests outside scope, then before two test inside scope
});

test('city database has Vienna', () => {
	expect(isCity('Vienna')).toBeTruthy();
});

test('city database has San Juan', () => {
	expect(isCity('San Juan')).toBeTruthy();
});

describe('matching cities to foods', () => {
	// Applies only to tests in this describe block
	beforeEach(() => {
		console.log('2-beforeEach');
		return initializeFoodDatabase();
	});

	test('Vienna <3 veal', () => {
		expect(isValidCityFoodPair('Vienna', 'Wiener Schnitzel')).toBe(true);
	});

	test('San Juan <3 plantains', () => {
		expect(isValidCityFoodPair('San Juan', 'Mofongo')).toBe(true);
	});
});
