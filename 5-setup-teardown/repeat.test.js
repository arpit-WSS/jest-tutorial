let city = [];

// function initializeCityDatabase() {
// 	city.push('Vienna');
// 	city.push('San Juan');
// }

// function clearCityDatabase() {
// 	return (city = []);
// }

//working with promise
function initializeCityDatabase() {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			city.push('Vienna');
			city.push('San Juan');
			resolve();
		}, 100);
	});
}

function clearCityDatabase() {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			city = [];
			resolve();
		}, 100);
	});
}

function isCity(name) {
	return city.includes(name);
}

beforeEach(() => {
	return initializeCityDatabase();
});

afterEach(() => {
	return clearCityDatabase(); // <------- when working with promise use return keyword
});

test('city database has Vienna', () => {
	expect(isCity('Vienna')).toBeTruthy();
});

test('city database has San Juan', () => {
	expect(isCity('San Juan')).toBeTruthy();
});

test('city database has San Juan', () => {
	expect(city.length).toBe(2);
});
