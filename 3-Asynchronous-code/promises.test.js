// To run async test, Return a promise from your test,
// and Jest will wait for that promise to resolve.
// If the promise is rejected, the test will automatically fail.

test('the data is peanut butter', () => {
	return fetchData().then((data) => {
		expect(data).toBe('peanut butter');
	});
});

// [Note]:  if you omit this return statement,
// your test will complete before the promise returned from fetchData resolves
// and then() has a chance to execute the callback.

// [Note]: If you expect a promise to be rejected, use the .catch method.
// [IMportant] : Make sure to add "expect.assertions" to verify that a certain number of assertions are called.
// Otherwise, a fulfilled promise would not fail the test.

test('the fetch fails with an error', () => {
	expect.assertions(1);
	return fetchData().catch((e) => expect(e).toMatch('error'));
});
