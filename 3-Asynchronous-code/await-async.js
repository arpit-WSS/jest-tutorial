// use asycn keyword before the function used in test

test('the data is peanut butter', async () => {
	const data = await fetchData();
	expect(data).toBe('peanut butter');
});

test('the fetch fails with an error', async () => {
	expect.assertions(1); // <------------------------------------------[IMPORTANT]
	try {
		await fetchData();
	} catch (e) {
		expect(e).toMatch('error');
	}
});

// await async with resolve and reject matchers
// replace return keyword with await

test('the data is peanut butter', async () => {
	await expect(fetchData()).resolves.toBe('peanut butter');
});

test('the fetch fails with an error', async () => {
	await expect(fetchData()).rejects.toMatch('error');
});
