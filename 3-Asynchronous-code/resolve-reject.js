// Use .resolve matcher in your expect statement
// Jest will wait for that promise to resolve.
// If the promise is rejected, the test will automatically fail.

test('data is peanut butter', () => {
	return expect(fetchData()).resolves.toBe('peanut butter');
});

//[Note]: dont omit return statement

// Use .reject matcher in your expect statement.
test('throw an error', () => {
	return expect(fetchData()).rejects.toThrow('error');
});
