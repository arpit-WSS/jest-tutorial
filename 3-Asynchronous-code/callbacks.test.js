// To work with async fucntion, Instead of putting the test in a function with an empty argument,
// use a single argument called done.
// Jest will wait until the done callback is called before finishing the test.

// [Note]: done() should not be mixed with Promises as this tends to lead to memory leaks in your tests.

test('the data is peanut butter', (done) => {
	function callback(data) {
		try {
			expect(data).toBe('peanut butter'); //if expect fails, done() will not be called.
			done(); // if done() is never called, it will give timeout error
		} catch (error) {
			done(error);
		}
	}
	fetchData(callback); //this function is not defined yet.
});
